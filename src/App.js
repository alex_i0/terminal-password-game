import React, { Component } from 'react';
import './App.css';
import Mainframe from './containers/Mainframe/Mainframe';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Mainframe />
      </div>
    );
  }
}

export default App;
