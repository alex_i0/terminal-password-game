import React, { Component } from 'react';
import words from '../../data/words';
import './Mainframe.css';

class Mainframe extends Component {

    state = {
        wordList: null,
        password: null,
        attempts: 5,
        hints: ["======PASSWORD REQUIRED======"]

    }

    componentWillMount() {
        this.createWordArr(16);
    }

    componentDidMount() {
        console.log(this.state.wordList);
        console.log(this.state.password);
    }

    createWordArr = (num) => {
        let wordArr = [];
        for (let i = 0; i < num; i++) {
            let randomIndex = Math.floor(Math.random() * words.length);
            wordArr.push(words[randomIndex]);
        }

        this.setState({
            wordList: wordArr
        });

        this.createPassword(wordArr);
    }

    createPassword = (arr) => {
        let randomIndex = Math.floor(Math.random() * arr.length);
        let password = arr[randomIndex];

        this.setState({
            password: password
        });
    }

    passwordCheckHandler = (target) => {
        const password = this.state.password;
        const word = target;

        console.log(`Pasword is ${password} and chosen word is ${word}`)

        this.isPasswordCorrect(password, target);
    }

    isPasswordCorrect = (pass, target) => {
        let hintArray = this.state.hints;
        let attempts = this.state.attempts;


        if (pass === target && this.state.attempts > 0) {

            hintArray.push(`>${target.toUpperCase()}`);
            hintArray.push(">Access Authorized.");
            this.setState({
                hints: hintArray,
                attempts: 0
            });

            console.log(">Access Authorized.");
        } else if (pass !== target && this.state.attempts > 0) {
            console.log("Access Denied");
            const splitedPassword = pass.split("");
            const splitedWord = target.split("");
            let likeness = 0;

            for (let i = 0; i < splitedWord.length; i++) {
                if (splitedWord[i] === splitedPassword[i]) {
                    likeness += 1;
                }
            }

            hintArray.push(`>${target.toUpperCase()}`);
            hintArray.push(">Entry denied.");
            hintArray.push(`>Likeness ${likeness}`);
            this.setState({
                hints: hintArray
            });
            console.log(`>Likeness=${likeness}`)

            this.setState({
                attempts: --attempts
            });
        }
    }

    attemptsCounter = (attempts) => {
        let blocks = "";

        for (var i = 0; i < attempts; i++) {
            blocks += "█ ";
        }

        console.log(blocks);

        return blocks;
    }



    render() {
        return (
            <div className="container">
                <div className="header">
                    <h1>Welcome to ROBCO Industries (TM) Termlink</h1>
                    <h2>Attempts Remaining: {this.attemptsCounter(this.state.attempts)}</h2>
                </div>

                <div className="game-console">

                    <div className="screen-display">
                        <div>{this.state.wordList.map((word, index) => {
                            return(
                                <React.Fragment>
                                    <div>0x49{index>= 10 ? index : "0" + index} <span className="single-word" key={index} onClick={() => this.passwordCheckHandler(word)}>{word.toUpperCase()} </span></div>
                                    <br /> 
                                </React.Fragment>  
                            );
                        })}</div>
                    </div>


                    <div className="hints-display">
                        <div>
                            {this.state.hints.map(
                                (hint, index) => {
                                    return <div key={index}>{hint}</div>
                                }
                            )}
                        </div>
                    </div>


                </div>
            </div>
        );
    }
}

export default Mainframe;